//
//  Constants.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation
import  UIKit

//MARK:- Global Constants

let numberOfTilesInARow = 3 // will be used as number of tiles in column as well
let padding : CGFloat = 10
let winningColor = UIColor(red:0.99,green:0.24,blue:0.22,alpha:1.00)



public struct Constants {
	//MARK:- User Defaults Keys
	/**
	*  Settings in NSUserDefaults for internal purpose
	*/
	struct DefaultsKeys {
		
		static let playerWins		= "PLAYER_WINS"
		static let playerLoss		= "PLAYER_LOST"
		static let gameDraw			= "GAME_DRAW"
	}
}


public enum AssetImage : String {
	case xImage = "x"
	case oImage = "o"
}


//MARK:- Enums
enum DifficultyLevel {
	case EASY
	case HARD
}


