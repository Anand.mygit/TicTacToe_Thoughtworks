//
//  AI.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation

///AI class can be used to create a intellegent player with a initial difficulty level
class AI {
	var difficultyLevel = DifficultyLevel.EASY
	var minimaxCall : Int = 0
	var scoreCall : Int = 0
	var choice : Int = -1
	var currentTurn = Turn.O
	
	
	init(difficultyLevel : DifficultyLevel) {
		self.difficultyLevel = difficultyLevel
	}
	
	func nextMoveIndex(currentState : State) -> Int{
		switch difficultyLevel {
		case .EASY:
			let availableMoves = currentState.emptyTileIndexes()
			let  randomMoveIndex = Utils.randomIntFrom(start: 0, to: availableMoves.count - 1 )
			print("Random \(randomMoveIndex), Available \(availableMoves)")
			return availableMoves[randomMoveIndex]
			
		case .HARD:
			
			_ =		minimax(currentState, player: currentTurn)
			return choice
			
		}
	}
	
	func minimax(_ state: State, player: Turn) -> Int {
		minimaxCall += 1
		
		// no more movements - terminal state
		if state.isGameCompleted() {
			// returns score for terminal position
			return score(state)
		}
		
		var scores : Array<Int> = []
		var moves : Array<Int> = []
		
		let a = state.emptyTileIndexes()
		for move in a
		{
			let next = State(oldState: state)
			next.set(index: move, turn:  player)
			
			// save score for next move
			scores.append(minimax(next, player : (player == .O ? .X : .O)))
			// save index of the next move
			moves.append(move)
		}
		
		// for player choose maximum from moves
		if currentTurn == player {
			let index = max(scores)
			choice = moves[index]
			return scores[index]
		}
		else // for opposite side calculate minimum
		{
			let index = min(scores)
			choice = moves[index]
			return scores[index]
		}
	}
	
	
	func score(_ board: State) -> Int {
		
		scoreCall += 1
		if board.isGameCompleted() {
			
			switch board.gameStatus {
				
			case .Won(let by, _):
				if by == currentTurn{
					return 10
				}
				else{
					return -10
				}
				
			default:
				return	0
			}
		}
		return	0
		
		
	}
	
	
	func min(_ array: Array<Int>) -> Int {
		var min = array[0]
		var result = 0
		for index in 1..<array.count {
			if (array[index] < min) {
				result = index;
				min = array[index]
			}
		}
		return result;
	}
	
	func max(_ array: Array<Int>) -> Int {
		var max = array[0]
		var result = 0
		for index in 1..<array.count {
			if (array[index] > max) {
				result = index;
				max = array[index]
			}
		}
		return result;
	}
	
	
	
	
	
	
	
	
	
	
}
