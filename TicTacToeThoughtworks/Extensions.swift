//
//  Extensions.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation
import UIKit

/*
Protocol extension of a UIView to get a UIView from NIB/XIB file.
Usages: - Give same name to the XIB and the Swift Class
*/
protocol UIViewLoading {}
extension UIView : UIViewLoading {}
extension UIViewLoading where Self : UIView {
	/// note that this method returns an instance of type `Self`, rather than UIView
	static func loadFromNib() -> Self {
		let nibName = "\(self)".characters.split{$0 == "."}.map(String.init).last!
		let nib = UINib(nibName: nibName, bundle: nil)
		return nib.instantiate(withOwner: self, options: nil).first as! Self
		
	}
	
}


/*
Protocol extension of a UIView to get a UIView from NIB/XIB file.
Usages: - Give same name to the XIB and the Swift Class
*/
protocol DataTypeConvertible {
	var cgFloatValue: CGFloat {get}
}
extension Int : DataTypeConvertible {
	internal var cgFloatValue: CGFloat {
		return CGFloat(self)
	}
}
extension Double : DataTypeConvertible {
	internal var cgFloatValue: CGFloat {
		return CGFloat(self)
	}
}

extension AssetImage {
	var image : UIImage {
		if let unwrappedImage = UIImage(named: self.rawValue){
			return unwrappedImage
		}
		else{
			print("ERROR: Asset with string '\(self.rawValue)' not found! [showing default image instead, Please verify asset string]")
			return UIImage(named: "errorImage")!
		}
	}
}
