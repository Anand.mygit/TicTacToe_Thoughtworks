//
//  GameViewController.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var aiModeButton: UIButton!
	@IBOutlet weak var gameView: UIView!
	@IBOutlet weak var statsLabel: UILabel!
	
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	var tiles = [TileCell]()
	var currentState = State(oldState: nil)
	var ai = AI(difficultyLevel: .HARD)
	var aiMovesCount = 0

	
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		tiles.removeAll()
		currentState = State(oldState: nil)
		aiModeButton.setTitle(String(describing: ai.difficultyLevel), for: UIControlState.normal)
		
		
		collectionView.dataSource = self
		collectionView.delegate = self
		collectionView.register(UINib(nibName: String(describing: TileCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: TileCell.self))
		
		updateStatLabels()
		activityIndicator.hidesWhenStopped = true


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
	
	@IBAction func selectMode(_ sender: Any) {
		selectAIMode()
		
	}
	
	func selectAIMode(){
		let alertController = UIAlertController(title: "Tic-Tac-Toe", message: "Please select difficulty level", preferredStyle: UIAlertControllerStyle.actionSheet)
		
		let easyAction = UIAlertAction(title: "EASY", style: UIAlertActionStyle.default, handler: {
			(action : UIAlertAction!) -> Void in
			self.ai = AI(difficultyLevel: .EASY)
			self.aiModeButton.setTitle(String(describing: self.ai.difficultyLevel), for: UIControlState.normal)
			
		})
		let hardAction = UIAlertAction(title: "HARD", style: UIAlertActionStyle.default, handler: {
			(action : UIAlertAction!) -> Void in
			self.ai = AI(difficultyLevel: .HARD)
			self.aiModeButton.setTitle(String(describing: self.ai.difficultyLevel), for: UIControlState.normal)
		})
		
		alertController.addAction(easyAction)
		
		alertController.addAction(hardAction)
		
		
		self.present(alertController, animated: true, completion: nil)
	}
	
	
	
	
	func aiAction(atIndex : Int){
		let nextState = State(oldState: currentState)
		nextState.state[atIndex] = .O
		tiles[atIndex].gameObject = nextState.state[atIndex]
		nextState.advanceTurn()
		currentState = nextState
	}
	
	func playerAction(atIndex : Int){
		if currentState.state[atIndex] == .Empty{
			
			let nextState = State(oldState: currentState)
			nextState.state[atIndex] = .X
			tiles[atIndex].gameObject = nextState.state[atIndex]
			nextState.advanceTurn()
			currentState = nextState
		}
	}
	
	func showGameStatus(){
		switch currentState.gameStatus {
		case .Won(let by, let tileIndexes):
			for i in 0..<numberOfTilesInARow{
				tiles[tileIndexes[i]].backgroundColor = winningColor
			}
			showAlert(message: "\(by.player()) Won")
			if by == .X{
				PersistentData.sharedInstance.playerWonCount += 1
				updateStatLabels()
			}
			else{
				PersistentData.sharedInstance.playerLostCount += 1
				updateStatLabels()
			}
			
		case .Draw:
			showAlert(message: "Game Draw")
			PersistentData.sharedInstance.gameDrawCount += 1
			updateStatLabels()
			
			
		default:
			print("Game Running")
			
		}
	}
	
	func showAlert(message : String)  {
		let alertController = UIAlertController(title: message, message: "Tap Restart button to play again", preferredStyle: UIAlertControllerStyle.actionSheet)
		
		let restartAction = UIAlertAction(title: "Restart", style: UIAlertActionStyle.default, handler: {
			(action : UIAlertAction!) -> Void in
			//				self.setUpBoard()
			self.resetGame()
		})
		
		
		alertController.addAction(restartAction)
		alertController.popoverPresentationController?.sourceView = tiles[4]
		
		present(alertController, animated: true, completion: nil)
		
	}
	
	func resetGame(){
		
		for cell in tiles{
			cell.gameObject = .Empty
			cell.backgroundColor = UIColor.white
			
		}
		currentState = State(oldState: nil)
		aiMovesCount = 0
		
	}
	
	func updateStatLabels(){
		statsLabel.text = "Won:  \(PersistentData.sharedInstance.playerWonCount) \nLost:  \(PersistentData.sharedInstance.playerLostCount) \nDraw: \(PersistentData.sharedInstance.gameDrawCount)"
	}


}


extension GameViewController : ActionDelegate{
	
	
	// Delegate method
	func clicked(tile: TileCell) {
		
		guard !currentState.isGameCompleted()  else {
			showGameStatus()
			return
		}
		
		playerAction(atIndex: tile.tag)
		guard !currentState.isGameCompleted() else {
			showGameStatus()
			return
		}
		
		if currentState.player == Turn.O.player() || currentState.state[tile.tag] == .Empty{
			activityIndicator.startAnimating()
			
			Utils.delayWithSeconds(0.1) {
				let aiNextMoveIndex = self.ai.nextMoveIndex(currentState: self.currentState)
				self.aiAction(atIndex: aiNextMoveIndex)
				self.activityIndicator.stopAnimating()

				guard !self.currentState.isGameCompleted() else {
					self.showGameStatus()
					return
				}
			}
		}
		
		
	}
	
	
}


extension GameViewController : UICollectionViewDataSource, UICollectionViewDelegate {
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return numberOfTilesInARow * numberOfTilesInARow
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TileCell.self), for: indexPath) as! TileCell
		
		tiles.append(cell)
		cell.delegate = self
		cell.tag = indexPath.row
		return cell
	}
	
}

// Delegate
extension GameViewController: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		
		let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
		let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfTilesInARow - 1))
		let size = (collectionView.bounds.width - totalSpace) / CGFloat(numberOfTilesInARow)
		return CGSize(width: size, height: size)
	}
	
}
