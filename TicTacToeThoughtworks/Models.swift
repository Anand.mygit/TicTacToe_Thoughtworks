//
//  Models.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation


///Represents a player's Turn 
enum Turn : String{
	case X = "You"
	case O = "AI"
	case Empty = "None"
	
	func player() -> String{
		return self.rawValue
	}
	
}



//At each given point game will have any of the following states
enum GameStatus : Equatable{
	case Running
	case Won(by: Turn, tileIndexes : [Int])
	case Draw
}

func ==(lhs: GameStatus, rhs: GameStatus) -> Bool {
	return String(stringInterpolationSegment: lhs) == String(stringInterpolationSegment: rhs)
}
