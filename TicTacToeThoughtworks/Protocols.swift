//
//  Protocols.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation

///Protocol for Player Action
protocol ActionDelegate {
	func clicked(tile: TileCell)
	
}
