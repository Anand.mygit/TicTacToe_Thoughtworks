//  State.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.

import Foundation

///At each given instance in the game, game board can be represented by a arrangement
class State {
	var state = [Turn]()
	var player = Turn.X.player()
	var aiMoveCount = 0
	var gameStatus = GameStatus.Running
	
	init(oldState : State?) {
		if let oldState = oldState{
			state = oldState.state
			player = oldState.player
			aiMoveCount = oldState.aiMoveCount
			gameStatus = oldState.gameStatus
		}
		else{
			for _ in 0..<9 {
				state.append(Turn.Empty)
			}
		}
	}
	
	func set(index : Int, turn : Turn){
		state[index] = turn
	}
	
	func emptyTileIndexes() -> [Int] {
		var emptyTileArray = [Int]()
		for i in 0..<9{
			if state[i] == .Empty{
				emptyTileArray.append(i)
			}
		}
		return emptyTileArray
	}
	
	func utility(){
		
		
	}
	
	func isGameCompleted() -> Bool {    // if Completed it returns Player (None if Draw), returns nil if its still running
		
		for i in stride(from: 0, to: 7, by: 3){
			
			if state[i] != .Empty &&  (state[i] == state[i+1]) && (state[i] == state[i+2]){
				
				gameStatus = GameStatus.Won(by: state[i], tileIndexes: [i,i+1,i+2])
				return true
			}
		}
		
		for i in 0...(numberOfTilesInARow - 1) {
			if state[i] != .Empty &&  (state[i] == state[i + numberOfTilesInARow]) && (state[i] == state[i+(2 * numberOfTilesInARow)]){
				gameStatus = GameStatus.Won(by: state[i], tileIndexes: [i,i + numberOfTilesInARow,i+(2 * numberOfTilesInARow)])
				return true
			}
		}
		

		let totalNumberOfTiles = (numberOfTilesInARow * numberOfTilesInARow)
		let centerTileIndex = (totalNumberOfTiles - 1) / 2
		var i =  numberOfTilesInARow + 1
		
		while i + centerTileIndex <= totalNumberOfTiles - 1 {
			if (state[centerTileIndex] != .Empty &&  state[centerTileIndex] == state[centerTileIndex + i] &&  state[centerTileIndex] == state[centerTileIndex - i] ){
				
				gameStatus = GameStatus.Won(by:  state[centerTileIndex], tileIndexes: [centerTileIndex - i , centerTileIndex, centerTileIndex + i])
				
				return true
			}
			i = i + numberOfTilesInARow + 1
			
		}
		
		var j = numberOfTilesInARow - 1
		while j + centerTileIndex <= totalNumberOfTiles - 1{
			if (state[centerTileIndex] != .Empty &&  state[centerTileIndex] == state[centerTileIndex + j] && state[centerTileIndex] == state[centerTileIndex - j] ){
				
				gameStatus = GameStatus.Won(by: state[centerTileIndex], tileIndexes: [centerTileIndex - j , centerTileIndex, centerTileIndex + j])
				
				return true
			}
			j = j + numberOfTilesInARow - 1
			
		}
		
		
		if emptyTileIndexes().count == 0{
			gameStatus = .Draw
			return true
		}
		
		return false
	}
	
	func advanceTurn(){
		player = (player == Turn.O.player()) ? Turn.X.player() : Turn.O.player()
		aiMoveCount += 1
		
	}
}
