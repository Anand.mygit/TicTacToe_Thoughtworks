//
//  TileCell.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import UIKit

class TileCell: UICollectionViewCell {
	
	var delegate: ActionDelegate?
	
	var gameObject = Turn.Empty {
		didSet {
			setImage(gameObject: gameObject)
		}
		
	}
	
	@IBOutlet weak var tileButton: UIButton!

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	

	@IBAction func tileButtonPressed(_ sender: Any) {
		delegate?.clicked(tile: self)
	}

	func setImage(gameObject : Turn){
		switch gameObject {
		case .O:
			tileButton.setImage(AssetImage.oImage.image, for: UIControlState.normal)
		case .X:
			tileButton.setImage(AssetImage.xImage.image, for: UIControlState.normal)
		default:
			tileButton.setImage(nil, for: UIControlState.normal)
		}
		
	}
}
