//
//  UserDefaults.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation


//MARK:- User Defaults
struct PersistentData {
	private let standardUserDefaults = UserDefaults.standard

	static var sharedInstance = PersistentData()
	private init() { print("Initialized Singleton")} //This prevents others from using the default '()' initializer for this class.
	
	var playerWonCount : Int {
		get{
			return (standardUserDefaults.value(forKey: Constants.DefaultsKeys.playerWins) as? Int) ?? 0
		}
		set{
			standardUserDefaults.setValue(newValue, forKey: Constants.DefaultsKeys.playerWins)
			standardUserDefaults.synchronize()
		}
		
	}
	
	var playerLostCount : Int {
		get{
			return (standardUserDefaults.value(forKey: Constants.DefaultsKeys.playerLoss) as? Int) ?? 0
		}
		set{
			standardUserDefaults.setValue(newValue, forKey: Constants.DefaultsKeys.playerLoss)
			standardUserDefaults.synchronize()
		}
	}
	
	var gameDrawCount : Int {
		get{
			return (standardUserDefaults.value(forKey: Constants.DefaultsKeys.gameDraw) as? Int) ?? 0
		}
		set{
			standardUserDefaults.setValue(newValue, forKey: Constants.DefaultsKeys.gameDraw)
			standardUserDefaults.synchronize()
		}
	}
}

