//
//  Utils.swift
//  TicTacToeThoughtworks
//
//  Created by Chetan Anand on 6/3/17.
//  Copyright © 2017 thoughtworks. All rights reserved.
//

import Foundation

class Utils {
	
	///Generates a Random integer in the range given
	class func randomIntFrom(start: Int, to end: Int) -> Int {
		var a = start
		var b = end
		// swap to prevent negative integer crashes
		if a > b {
			swap(&a, &b)
		}
		return Int(arc4random_uniform(UInt32(b - a + 1))) + a
	}
	
	
	/// Perform delay with completion block
	class func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
		DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
			completion()
		}
	}
}
